# Kodi Spreadsheet Sync
This python script gets movie information from a kodi instance and then enters it into a Google Spreadsheet.

## Usage
1. Install the needed dependencies using `pip install -r requirements.txt`.
2. Run `./src/script.py`

## Configuration
Inside of `script.py` you can find numerous configuration options you can change to your liking.

In addition you need to get your `credentials.json` from the Google API console and put it next to script.py to get this script working. See the documentation from gspread for further information:
[https://gspread.readthedocs.io/en/latest/oauth2.html](https://gspread.readthedocs.io/en/latest/oauth2.html)