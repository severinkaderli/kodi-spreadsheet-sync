#!/usr/bin/env python3
import datetime
import gspread
from kodipydent import Kodi
from oauth2client.service_account import ServiceAccountCredentials

#######################################
#
# Configuration variables
#
#######################################
# The JSON file which is used for authentication
# it needs to be an absolute path to work everywhere.
CREDENTIALS_FILE = "/home/osmc/kodi-spreadsheet-sync/credentials.json"

# The access scope for the API
SCOPE = ["https://spreadsheets.google.com/feeds"]

# The key of the spreadsheet
SPREADSHEET_KEY = "1mG9C35SwEPEmURsRvcF2Brl6UrvHagvjn3MMj35SbtQ"

# The starting row for movie entries
START_ROW = 2

# The index of the worksheet which should be used for movies
MOVIES_WORKSHEET_INDEX = 0

# The index of the worksheet which should be used for tv shows
SHOWS_WORKSHEET_INDEX = 1

# The url for the kodi api
KODI_URL = "127.0.0.1"

# The port for the kodi api
KODI_PORT = 8080

# The properties which should be get from kodi for movies
MOVIE_PROPERTIES = ["year", "userrating",
                    "imdbnumber", "playcount"]

# The properties which should be get from kodi for tv shows
SHOW_PROPERTIES = ["year", "userrating",
                   "imdbnumber", "episode", "watchedepisodes", "season"]


#######################################
#
# The actual code. You don't need to change anything after here.
#
#######################################
def log_message(message):
    """Logs a message with the current date and time."""
    current_datetime = datetime.datetime.now()
    print(current_datetime.strftime("%Y-%m-%d %H:%M") + ": " + message)


def get_range(start_column, start_row, end_column, end_row):
    """Gets a range for the given column and rows."""
    start_letter = chr(start_column - 1 + ord("A"))
    end_letter = chr(end_column - 1 + ord("A"))
    return "{}{}:{}{}".format(start_letter, start_row, end_letter, end_row)


def clear_worksheet(worksheet):
    """Clears all cells in the given worksheet except the first row."""
    log_message("Clearing worksheet {}".format(worksheet.title))
    number_of_rows = worksheet.row_count
    cell_list = worksheet.range(get_range(1, START_ROW, 7, number_of_rows))
    for cell in cell_list:
        cell.value = ""
    worksheet.update_cells(cell_list)


def get_watched_icon(watched):
    """Returns the correct unicode character depending on the watch status."""
    return "✔" if watched else "🗙"


def update_movies(kodi_client, spreadsheet):
    # Get the movies from kodi
    log_message("Getting movie list from Kodi")
    movies = kodi_client.VideoLibrary.GetMovies(properties=MOVIE_PROPERTIES)
    movies = movies["result"]["movies"]
    movies = sorted(movies, key=lambda movie: movie["label"])

    # Get the correct spreadsheet
    worksheet = spreadsheet.get_worksheet(MOVIES_WORKSHEET_INDEX)
    clear_worksheet(worksheet)

    # Loop through the movies and them to the spreadsheet
    log_message("Adding movies to spreadsheet")
    row_cell_list = worksheet.range(
        get_range(1, START_ROW, 5, len(movies) + 1))
    current_index = 0
    for movie in movies:
        row_cell_list[current_index * 5].value = movie["label"]
        row_cell_list[current_index * 5 + 1].value = movie["year"]
        row_cell_list[current_index * 5 + 2].value = get_watched_icon(
            movie["playcount"]
        )
        row_cell_list[current_index * 5 + 3].value = movie["userrating"]
        row_cell_list[current_index * 5 + 4].value = (
            "https://www.imdb.com/title/" + movie["imdbnumber"] + "/"
        )
        current_index += 1

    # Update the rows
    worksheet.update_cells(row_cell_list)


def update_shows(kodi_client, spreadsheet):
    # Get the movies from kodi
    log_message("Getting show list from Kodi")
    shows = kodi_client.VideoLibrary.GetTVShows(properties=SHOW_PROPERTIES)
    shows = shows["result"]["tvshows"]
    shows = sorted(shows, key=lambda show: show["label"])

    # Get the correct spreadsheet
    worksheet = spreadsheet.get_worksheet(SHOWS_WORKSHEET_INDEX)
    clear_worksheet(worksheet)

    # Loop through the movies and them to the spreadsheet
    log_message("Adding shows to spreadsheet")
    row_cell_list = worksheet.range(get_range(1, START_ROW, 7, len(shows) + 1))
    current_index = 0
    for show in shows:
        row_cell_list[current_index * 7].value = show["label"]
        row_cell_list[current_index * 7 + 1].value = show["year"]
        row_cell_list[current_index * 7 + 2].value = show["season"]
        row_cell_list[current_index * 7 + 3].value = show["episode"]
        row_cell_list[current_index * 7 + 4].value = show["watchedepisodes"]
        row_cell_list[current_index * 7 + 5].value = show["userrating"]
        row_cell_list[current_index * 7 + 6].value = (
            "https://www.thetvdb.com/?tab=series&id=" +
            show["imdbnumber"] + "/"
        )
        current_index += 1

    # Update the rows
    worksheet.update_cells(row_cell_list)


if __name__ == "__main__":
    # Connect to Google Spreadsheets and retrieve the spreadsheet
    log_message("Connecting to Google Spreadsheets")
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        CREDENTIALS_FILE, SCOPE
    )
    gc = gspread.authorize(credentials)
    spreadsheet = gc.open_by_key(SPREADSHEET_KEY)

    # Connecting to Kodi
    log_message("Connecting to Kodi")
    kodi_client = Kodi(KODI_URL, KODI_PORT)

    # Update worksheets
    update_movies(kodi_client, spreadsheet)
    update_shows(kodi_client, spreadsheet)
